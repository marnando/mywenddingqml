import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2

ApplicationWindow {
    id: root
    title: qsTr("My Wendding")
    width: 540
    height: 960
    visible: true

    Row {
        id: m_rowMenu
        height: 96

        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0

        Button {
            id: m_btnBack
            width: 96
            height: 96
            visible: false
            Image {
                id: m_backIcon
                width: parent.width - 5
                height: parent.height - 5
                source: "images/48px/back.png"
            }
            anchors.left: parent.left
            anchors.leftMargin: 0
        }

        Text {
            id: m_textCouple
            height: 96
            text: qsTr("Junior e Natty")
            font.pixelSize: 36
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft

            anchors.right: m_btnConfig.left
            anchors.rightMargin: 116
            anchors.left: m_btnBack.right
            anchors.leftMargin: 10
        }

        Button {
            id: m_btnList
            width: 96
            height: 96
            Image {
                id: m_listIcon
                width: parent.width - 5
                height: parent.height - 5
                source: "images/48px/List.png"
            }
            anchors.right: m_btnConfig.left
            anchors.rightMargin: 10
        }

        Button {
            id: m_btnEdit
            width: 96
            height: 96
            visible: false

            Image {
                id: m_editIcon
                width: parent.width - 5
                height: parent.height - 5
                source: "images/48px/add_prof.png"
            }

            anchors.right: m_btnConfig.left
            anchors.rightMargin: 10
        }

        Button {
            id: m_btnConfig
            width: 96
            height: 96

            Image {
                id: m_SettingsIcon
                width: parent.width - 5
                height: parent.height - 5
                source: "images/48px/config.png"
            }

            anchors.right: parent.right
            anchors.rightMargin: 0
        }
    }

    Rectangle {
        id: m_dockWidgets
        anchors.left: m_rowMenu.right
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.right: m_rowMenu.left
        anchors.rightMargin: 0
        anchors.top: m_rowMenu.bottom
        anchors.topMargin: 0


        Loader {
            id: m_dockLoader
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            anchors.leftMargin: 0
            anchors.topMargin: 0
            sourceComponent: m_listPorfissions
            anchors.fill: parent
        }

        Component {
            id: m_listPorfissions

            ListProfissions {
                anchors.fill: parent
                color: "#236B8E"
            }
        }
    }
}
