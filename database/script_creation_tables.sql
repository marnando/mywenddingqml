
CREATE TABLE t_tvar(u_id INTEGER PRIMARY KEY ASC
                  , u_id_professional INT
                  , id_financial INT
                  , d_valor REAL
                  , u_status INT);                  

CREATE TABLE t_financial(u_id INTEGER PRIMARY KEY ASC NOT NULL
                       , u_id_professional INT NOT NULL
                       , d_total_price REAL NOT NULL
                       , d_penalty REAL NOT NULL
                       , d_penaulty_deadline REAL NOT NULL
                       , u_status INT NOT NULL);                       

CREATE TABLE t_professional(u_id INTEGER PRIMARY KEY ASC NOT NULL
                          , s_nome CHAR NOT NULL
                          , u_rg INT
                          , u_cpf INT
                          , u_cnpj INT
                          , s_adress CHAR
                          , s_type CHAR NOT NULL
                          , u_fone_1 INT NOT NULL
                          , u_fone_2 INT NOT NULL
                          , u_fone_3 INT
                          , u_status INT NOT NULL);                          

CREATE TABLE t_price(u_id INTEGER PRIMARY KEY ASC NOT NULL
                   , u_id_financial INT NOT NULL
                   , d_entry REAL
                   , d_descount_1 REAL
                   , d_descount_2 REAL
                   , s_method CHAR NOT NULL);                   

CREATE TABLE t_term_payment(u_id INTEGER PRIMARY KEY ASC NOT NULL
                          , u_id_price INT NOT NULL
                          , u_installment INT
                          , u_term_payment_total INT NOT NULL
                          , d_initial INT
                          , d_final INT
                          , u_status INT NOT NULL); 


