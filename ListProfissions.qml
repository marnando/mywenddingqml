import QtQuick 2.0

Rectangle  {
    color: "#236B8E"
    width: 300
    height: 400
    anchors.fill: parent

    VisualItemModel  {
        id: itemModel

        Rectangle {
            id: mLogin
            color: "red"
            Text {
                id: textLogin
                text: qsTr("Login")
                anchors.centerIn: parent
            }
            width: parent.parent.width
            height: parent.parent.height
        }

        Rectangle {
            id: mCadastro
            color: "green"
            Text {
                id: textCad
                text: qsTr("Cadastro")
                anchors.centerIn: parent
            }
            width: parent.parent.width
            height: parent.parent.height
        }
    }

    ListView {
        id: view
        layoutDirection: Qt.LeftToRight
        model: itemModel

        anchors  {
            fill: parent;
            bottomMargin: 30
        }

        preferredHighlightBegin: 0
        preferredHighlightEnd: 0

        highlightMoveDuration: 250
        highlightRangeMode: ListView.StrictlyEnforceRange
        boundsBehavior: Flickable.StopAtBounds

        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
    }
}

